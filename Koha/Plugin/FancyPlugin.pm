package Koha::Plugin::FancyPlugin;

use Modern::Perl;

use base qw(Koha::Plugins::Base);

our $VERSION = "{VERSION}";

our $metadata = {
    name            => 'Our fancy plugin',
    author          => 'Your name',
    description     => 'Some useful description, think of end users!',
    date_authored   => '2020-12-01',
    date_updated    => "1970-01-01",
    minimum_version => '19.1100000',
    maximum_version => undef,
    version         => $VERSION,
};

sub new {
    my ( $class, $args ) = @_;

    $args->{'metadata'} = $metadata;
    my $self = $class->SUPER::new($args);

    return $self;
}

sub opac_head {
    my ( $self ) = @_;

    my $color = $self->retrieve_data('color');
    return qq{
      <style>
        body {background-color: $color;}
      </style>
    };
}

sub configure {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    unless ( $cgi->param('save') ) {
        my $template = $self->get_template({ file => 'configure.tt' });

        ## Define a list of color options
        $template->param(
            options => [ 'pink', 'red', 'orange', 'blue' ],
        );

        ## Grab the values we already have for our settings, if any exist
        $template->param(
            color => $self->retrieve_data('color'),
        );

        $self->output_html( $template->output() );
    }
    else {
        $self->store_data(
            {
                color => scalar $cgi->param('color'),
            }
        );
        $self->go_home();
    }
}

sub install {
    my ( $self, $args ) = @_;

    my $words_list_table = $self->get_qualified_table_name('words_list');

    C4::Context->dbh->do(qq{
        CREATE TABLE $words_list_table (
          `id` INT(11) NOT NULL auto_increment,
          `fancy_word` NULL DEFAULT NULL,
          `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY  (id),
          CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
    }) unless $self->_table_exists($words_list_table);

    return 1;
}

sub upgrade {
    my ( $self, $args ) = @_;

    my $database_version = $self->retrieve_data('__INSTALLED_VERSION__') || 0;

    if ( $self->_version_compare( $database_version, "0.0.1" ) == -1 ) {

        my $words_list_table = $self->get_qualified_table_name('words_list');

        C4::Context->dbh->do(qq{
            ALTER TABLE $words_list_table
            ADD COLUMN `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
            AFTER `fancy_word`;
        });

        $database_version = "0.0.2";
        $self->store_data({ '__INSTALLED_VERSION__' => $database_version });
    }

    return 1;
}

sub _table_exists {
    my $table = shift;
    eval {
        C4::Context->dbh->{PrintError} = 0;
        C4::Context->dbh->{RaiseError} = 1;
        C4::Context->dbh->do(qq{SELECT * FROM $table WHERE 1 = 0 });
    };
    return 1 unless $@;
    return 0;
}
